package sport;

/**
 * Created by Admin on 23.10.2016.
 */
public class Sport<k extends Discipline,v extends Constituent> {
    k discipline;
    v constituent;
    Sport(k discipline, v constituent ){
        this.constituent=constituent;
        this.discipline=discipline;
    }
    public void cantLiveWithout(){
        System.out.println(discipline+" It can't live without "+constituent);
        ///System.out.print(" and fans");
    }
}
