package sport;

/**
 * Created by Admin on 23.10.2016.
 */
public class Run {
    public static void main(String[]args){
        Football football = new Football("Football", "Game description.");
        Formula formula= new Formula("Formula", "Game description.");
        Hockey hockey=new Hockey("Hockey", "Game description.");

        Fans fans = new Fans("Passion fans.");
        Ball ball=new Ball("Ball");
        Ice ice=new Ice("Ice");
        Car car=new Car("Car");

        Sport<Football, Ball> footballFirstPassion = new Sport<>(football, ball);
        footballFirstPassion.cantLiveWithout();

        Sport<Hockey, Ice> hockeyFirstPassion = new Sport<>(hockey, ice);
        hockeyFirstPassion.cantLiveWithout();

        Sport<Formula, Car> formulaFirstPassion = new Sport<>(formula, car);
        footballFirstPassion.cantLiveWithout();
    }
}
