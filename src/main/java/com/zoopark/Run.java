package com.zoopark;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by org2 on 21.10.2016.
 */
public class Run {
    public static void main(String[] args) {
        Bear bear = new Bear("Mishka");
        ZooBox boxforbear = new ZooBox();
        boxforbear.lockAnimal(bear);

        Monkey monkey = new Monkey("Humen");
        ZooBox boxformonkey = new ZooBox();
        boxformonkey.lockAnimal(monkey);

        Shark shark = new Shark("Akula");
        ZooBox boxforshark = new ZooBox();
        boxforshark.lockAnimal(shark);

        Zebra zebra = new Zebra("konyaka");
        ZooBox boxforzebra = new ZooBox();
        boxforzebra.lockAnimal(zebra);

        ArrayList<ZooBox> list = new ArrayList<ZooBox>(4);
        list.add(boxforbear);
        list.add(boxformonkey);
        list.add(boxforshark);
        list.add(boxforzebra);
        for (int i = 0; i < list.size(); i++) {
            list.get(i).getAnimal().sayName();
        }


    }
}
