package com.zoopark;

/**
 * Created by org2 on 21.10.2016.
 */
public class Monkey extends Animal {
    Monkey(String name) {
        this.animalType = AnimalType.MAMMAL;
        this.name = name;
    }

    @Override
    void sayName() {
        System.out.println("I'm a" + animalType + ", my name is " + name);
    }
}
