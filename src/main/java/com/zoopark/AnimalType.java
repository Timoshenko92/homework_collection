package com.zoopark;

/**
 * Created by org2 on 21.10.2016.
 */
public enum AnimalType {
    MAMMAL, AMPHIBIAN, BIRD, FISH;
}
