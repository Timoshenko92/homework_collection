package com.zoopark;

/**
 * Created by org2 on 21.10.2016.
 */
public class Shark  extends Animal{
    Shark (String name){
        this.animalType=AnimalType.FISH;
        this.name=name;
    }
    @Override
    void sayName() {
        System.out.println("I'm a"+animalType+", my name is "+name);
    }
}
