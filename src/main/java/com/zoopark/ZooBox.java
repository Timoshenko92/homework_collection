package com.zoopark;

/**
 * Created by org2 on 21.10.2016.
 */
public class ZooBox<T extends Animal> {
    private T animal;

    public void lockAnimal(T animal) {
        this.animal = animal;
    }

    public  T getAnimal () {
        return animal;
    }
}
